#!/usr/bin/env bash

## Shell options
set -e -u -x

# Enable CodeReady Linux Builder repository
echo -e "\nEnabling CodeReady Linux Builder repository...\n"
sudo subscription-manager repos --enable "codeready-builder-for-rhel-8-x86_64-rpms"

# Install EPEL repository
echo -e "\nEnabling EPEL repository...\n"
sudo dnf install -y "https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm"
# Enable EPEL repository
sudo dnf config-manager --set-enabled epel

# Install Ansible
echo -e "\nInstalling Ansible...\n"
sudo dnf install -y ansible-collection-community-general

# Install Git
echo -e "\nInstalling Git...\n"
sudo dnf install -y git

# Clone Ansible playbook
echo -e "\nCloning and setting up Ansible provisioning playbook...\n"
git clone https://gitlab.com/penyuan/ansible-rhel.git
# Pull submodules for Ansible roles
cd ansible-rhel
git submodule init && git submodule update

# Run Ansible to provision this system
echo -e "\nRunning Ansible...\n"
ansible-playbook setup.yml -v -i HOSTS --ask-become-pass
