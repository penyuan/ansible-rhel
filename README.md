# ansible-rhel

Ansible playbook to provision a fresh Red Hat Enterprise Linux 8 (RHEL 8) system with basic packages and customisation.

## Preparation

Before running Ansible, bootstrap a freshly-installed RHEL 8 system with the following steps. It is assumed that everything will be run as a local user with sudo privileges.

### Enable basic repositories

Enable CodeReady Linux Builder repo in as a dependency for EPEL repo: 

`sudo subscription-manager repos --enable "codeready-builder-for-rhel-8-x86_64-rpms"`

Set up EPEL repo: 

`sudo dnf install "https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm"`

Enable EPEL repo: 

`sudo dnf config-manager --set-enabled epel`

### Set up Ansible

Install the `ansible-collection-community-general` package from EPEL, which would also pull in the base Ansible package as a dependency: 

`sudo dnf install ansible-collection-community-general`

Information about what's in the Ansible Community General Collection of modules and plugins: 

https://galaxy.ansible.com/community/general

### Install Git

`sudo dnf install git`

### set up $HOME/provisioning-local/

### get Gnome Shell Extensions Ansible role

`cd $HOME/provisioning-local/`
`git clone https://github.com/jaredhocutt/ansible-gnome-extensions.git`

### Run Ansible playbook

`ansible-playbook setup.yml -v -i HOSTS --ask-become-pass`

### Search for and add other OSes on the machine to grub menu

* Mount the partition(s) containing other OSes, like Windows.

### Other possible things to install

* Anaconda
* AppImageLauncher: https://github.com/TheAssassin/AppImageLauncher
  * AppImage desktop integration and manager
* [asdf](https://asdf-vm.com/) which supports R and Python
  * R
    * Plugin: https://github.com/asdf-community/asdf-r
    * Prerequisites for building R with asdf R plugin `sudo dnf install bzip2-devel gcc-gfortran libcurl-devel pcre2-devel readline-devel libXt-devel xz-devel` (for why this particular xorg package `libXt-devel`, see: https://unix.stackexchange.com/a/346561), adapted from: https://github.com/asdf-community/asdf-r#linux
    * Install with `--enable-R-shlib --with-cairo` (especially `--enable-R-shlib` which makes it accessible by RStudio): https://github.com/asdf-community/asdf-r#building-r-shared-library
      * Specifically, the command to run is `R_EXTRA_CONFIGURE_OPTIONS="--enable-R-shlib --with-cairo" asdf install R latest`, see example: https://github.com/smashedtoatoms/asdf-postgres#asdf-options
    * Also reference RStudio's recommendations for building R: https://docs.rstudio.com/resources/install-r-source/
  * Python
    * Plugin: https://github.com/danhper/asdf-python
    * Python build dependencies `dnf install make gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel` from: https://github.com/pyenv/pyenv/wiki#suggested-build-environment
* Conty.sh
* distrobox
* fonts
  * https://access.redhat.com/solutions/3070981
  * https://github.com/source-foundry/Hack#quick-installation
  * https://access.redhat.com/solutions/124613
* Homebrew
  * ffmpeg --HEAD
  * hugo
  * pandoc
* Deta CLI
* Heroku CLI
* symlink docker to podman
* czkawka
* Pomotroid
* yt-dlp
* LosslessCut
* [Insomnia](https://insomnia.rest/products/insomnia) API design and query tool
* JuNest
* pyenv
* SSH configuration
* stubby
* Tor Browser
* NoiseTorch
* Audacity
* Openshot or Kdenlive or Shotcut
* MusicBrainz Picard
* Zettlr
* Remmina
* Skype
* Rstudio
  * For builds for RHEL 9 or just latest daily builds in general, go here: https://dailies.rstudio.com/rstudio/
* Cryptomator

### VirtualBox

Changes in commit `00da6783219e75f450d511c96ae49a2485905513` on 2022-04-18 imply that copy/paste (bidirectional clipboard) doesn't seem to work in the `VirtualBox` packages installed from RPM Fusion, and as discussed here: https://forums.virtualbox.org/viewtopic.php?f=2&t=105791&p=516392 
However, after some testing, including installing the official `VirtualBox-6.1` packages from the official VirtualBox repository, I discovered that this is not the problem. In fact, it is because I started my Windows 10 guest in "Detachable Start" mode instead of "Normal" mode. So maybe installing the package from RPM Fusion is fine after all.

### Gnome settings

Run this command to see changes to Gnome dconf settings as they happen: `dconf watch /`

This helps figure out what to put in `dconf.yml`.

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/penyuan/ansible-rhel.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://gitlab.com/penyuan/ansible-rhel/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:3a806372a72cf586034bc6a29598a839?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

